Feature: OpenCart Login 

Background: Background process
 Given user navigate to login page
   

@ValidLogin
  Scenario Outline: Login with valid credentials
   When enters <username> and <password>
    And click on login button
    Then verfiy user entering <PIN> number
    Then verify user should login successfully.
    Then user logout successfully

    Examples: 
      | username             | password     |PIN|
      | Sai.Potdar@yahoo.com | OpenCart@123 |2020|

@InvalidLogin
 Scenario Outline: Login with invalid credentials
    When enters <username> and <password>
    And click on login button
    Then verify user should not login successfully.

    Examples: 
      | username             | password     |PIN|
      | Sai.Potdar1@yahoo.com | OpenCart@123 |2020|

      
      