Feature: Update Account Details

Background: Background process
 Given user navigate to login page
 
@UpdateAccount
  Scenario: Update users account details
       #Using List data table
    When user enters credentials
      | Sai.Potdar@yahoo.com | OpenCart@123 |
      
    And click on login button
    Then user entering PIN number
      | 2020 |
      
    When verify user should login successfully.
    And user clicks on edit account details
    #Using Map data table
    Then user enters new details
      | username | sai.potdar1234 |
      | country  | Hong Kong      |
      
    And user clicks on submit
    Then user logout successfully
