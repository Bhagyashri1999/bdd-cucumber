Feature: Add store information

Background: Background process
    Given user navigate to login page
  
@AddStoreInformation
  Scenario Outline: Add domain in store information
    When enters <username> and <password>
    And click on login button
    Then verfiy user entering <PIN> number
    Then verify user should login successfully.
    And navigate to your store tab
    Then delete existing store information
    And add new store information
    Then user logout successfully

    Examples: 
      | username             | password     | PIN  |
      | Sai.Potdar@yahoo.com | OpenCart@123 | 2020 |
