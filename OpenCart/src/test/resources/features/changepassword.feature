Feature: Change password

Background: user should loged in
    Given user navigate to login page
    When enters Sai.Potdar@yahoo.com and OpenCart@123
    And click on login button
       
@ChangePassword
  Scenario Outline: change account password
    Then verfiy user entering <PIN> number
    Then verify user should login successfully.
    And navigate to change password tab
    Then user enters <currentPassword> and <newPassword>
    And clicks on continue button
    Then verify password changed successfully
    Then user logout successfully

    Examples: 
       | PIN  | currentPassword | newPassword  |
       | 2020 | OpenCart@123    | OpenCart@123 |
