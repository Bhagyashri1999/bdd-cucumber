package com.opencart.stepDefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.opencart.pages.AccountPage;
import com.opencart.pages.ConfirmPINPage;
import com.opencart.pages.EditAccountDetailsPage;
import com.opencart.pages.LoginPage;
import com.webshop.utilities.SetupBrowser;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class UpdateAccountDetails extends SetupBrowser {
	WebDriver driver = setup();

	LoginPage loginPageObj;
	ConfirmPINPage confirmPinPageObj;
	AccountPage accountPageObj;
	EditAccountDetailsPage editAccDetailsPageObj;

	@Then("^user entering PIN number$")
	public void user_entering_PIN_number(DataTable data) throws Throwable {

		confirmPinPageObj = new ConfirmPINPage(driver);
		List<List<String>> pinNumber = data.raw();
		confirmPinPageObj.setPINNumber(pinNumber.get(0).get(0));
		confirmPinPageObj.clickSubmitButton();

	}

	@When("^user enters credentials$")
	public void user_enters_username_and_password(DataTable data) throws Throwable {

		List<List<String>> credentials = data.raw();
		loginPageObj = new LoginPage(driver);
		loginPageObj.setUsernameTxt(credentials.get(0).get(0));
		loginPageObj.setPasswordTxt(credentials.get(0).get(1));

	}

	@When("^user clicks on edit account details$")
	public void user_clicks_on_edit_account_details() throws Throwable {

		accountPageObj = new AccountPage(driver);
		accountPageObj.clickEditAccountTab();

	}

	@Then("^user enters new details$")
	public void user_enters_new_details(DataTable data) throws Throwable {

		Map<String, String> details = data.asMap(String.class, String.class);

		editAccDetailsPageObj = new EditAccountDetailsPage(driver);
		editAccDetailsPageObj.setUsername(details.get("username"));
		editAccDetailsPageObj.setCountry(details.get("country"));

	}

	@And("^user clicks on submit$")
	public void user_clicks_on_submit() throws Throwable {

		editAccDetailsPageObj.clickSubmitButton();

	}



}
