package com.opencart.stepDefinitions;

import org.openqa.selenium.WebDriver;

import com.opencart.pages.AccountPage;
import com.opencart.pages.ConfirmPINPage;
import com.opencart.pages.LoginPage;
import com.opencart.pages.YourStorePage;
import com.webshop.utilities.FileReaderUtilitiy;
import com.webshop.utilities.SetupBrowser;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class AddStoreInformation extends SetupBrowser {
WebDriver driver=setup();
	
   AccountPage accountPageObj;
   YourStorePage yourStorePageObj;
   FileReaderUtilitiy fileReaderUtilitiyObj=new FileReaderUtilitiy();
	
   @And("^navigate to your store tab$")
   public void navigate_to_your_store_tab() throws Throwable {
	   
	   accountPageObj=new AccountPage(driver);
	   accountPageObj.clickYourStoreTab();
       
   }

   @Then("^delete existing store information$")
   public void delete_existing_store_information() throws Throwable {
	   yourStorePageObj=new YourStorePage(driver);
	   yourStorePageObj.clickDeleteButton();
	   yourStorePageObj.handlePopUpWindow();
   }

   @And("^add new store information$")
   public void add_new_store_information() throws Throwable {
	  
	   yourStorePageObj.clickAddButton();
       yourStorePageObj.enterDomain(fileReaderUtilitiyObj.getDomain());
       yourStorePageObj.clickSubmitButton();
   }
}
