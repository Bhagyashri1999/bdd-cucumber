package com.opencart.stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.opencart.pages.AccountPage;
import com.opencart.pages.ConfirmPINPage;
import com.opencart.pages.HomePage;
import com.opencart.pages.LoginPage;
import com.webshop.utilities.FileReaderUtilitiy;
import com.webshop.utilities.SetupBrowser;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login extends SetupBrowser {

	protected static WebDriver driver = setup();
	WebElement message;
	FileReaderUtilitiy fileReaderUtilitiy = new FileReaderUtilitiy();

	LoginPage loginPageObj;
	HomePage homePageObj;
	ConfirmPINPage confirmPinPageObj;
	AccountPage accountPageObj;

	@Before()
	public void openURL() throws InterruptedException {
		String url = fileReaderUtilitiy.getUrl();
		driver.navigate().to(url);
		

	}
	
	@Given("^user navigate to login page$")
	public void user_navigate_to_login_page() throws Throwable {

		homePageObj = new HomePage(driver);
		homePageObj.clickLoginLink();

	}

	@When("^enters (.*) and (.*)$")
	public void enters_username_and_password(String username, String password) throws Throwable {

		loginPageObj = new LoginPage(driver);
		loginPageObj.setUsernameTxt(username);
		loginPageObj.setPasswordTxt(password);
	}

	@And("^click on login button$")
	public void click_on_login_button() throws Throwable {

		loginPageObj = new LoginPage(driver);
		loginPageObj.clickLoginButton();
	}

	@Then("^verfiy user entering (.*) number$")
	public void verfiy_user_entering_PIN(String pinNumber) throws Throwable {

		confirmPinPageObj = new ConfirmPINPage(driver);
		confirmPinPageObj.setPINNumber(pinNumber);
		confirmPinPageObj.clickSubmitButton();
	}

	@Then("^verify user should login successfully\\.$")
	public void verify_user_should_login_successfully() throws Throwable {

		accountPageObj = new AccountPage(driver);
		accountPageObj.verifyLoginMessage();

	}

	@Then("^verify user should not login successfully\\.$")
	public void verify_user_should_not_login_successfully() throws Throwable {

		loginPageObj.verifyInvalidLoginMsg();

	}

	@Then("^user logout successfully$")
	public void user_logout_successfully() throws Throwable {

		accountPageObj = new AccountPage(driver);
		accountPageObj.clickLogoutButton();
	}
	
	
}
