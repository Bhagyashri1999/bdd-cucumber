package com.opencart.stepDefinitions;

import org.openqa.selenium.WebDriver;

import com.opencart.pages.AccountPage;
import com.opencart.pages.ChangePasswordPage;
import com.webshop.utilities.SetupBrowser;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ChangePassword extends SetupBrowser{

 WebDriver driver=setup();
	
	AccountPage accountPageObj;
	ChangePasswordPage changePasswordPageObj;

	@And("^navigate to change password tab$")
	public void navigate_to_change_password_tab() throws Throwable {
	  
		accountPageObj=new AccountPage(driver);
		accountPageObj.clickChangePasswordTab();
		
	    
	}

	@Then("^user enters (.*) and (.*)$")
	public void user_enters_current_and_new_password(String currentPassword,String newPassword) throws Throwable {
	    
		changePasswordPageObj=new ChangePasswordPage(driver);
		changePasswordPageObj.enterCurrentPassword(currentPassword);
		changePasswordPageObj.enterNewPassword(newPassword);
		changePasswordPageObj.confirmNewPassword(newPassword);
	}

	@And("^clicks on continue button$")
	public void clicks_on_continue_button() throws Throwable {
	   
	    changePasswordPageObj.clickContinueButton();
	}

	@Then("^verify password changed successfully$")
	public void verify_password_changed_successfully() throws Throwable {
	    
	    accountPageObj.verifyPasswordChange();
	}
}
