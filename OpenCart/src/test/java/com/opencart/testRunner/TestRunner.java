package com.opencart.testRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
			features="src/test/resources/features",
			glue= {"com.opencart.stepDefinitions"},
			format= {"pretty","html:test-report/htmlReport.html"},
			tags= {"@ValidLogin"}
			)

public class TestRunner {

}
