package com.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	WebDriver driver;
	
	@FindBy(xpath="//a[@href='https://www.opencart.com/index.php?route=account/login' and @class='btn btn-link navbar-btn']")
	WebElement loginLink;
	
	@FindBy(xpath="//div[@class='navbar-right hidden-xs']//following::a[contains(text(),'Register')]")
	WebElement registerLink;
	
	@FindBy(xpath="//img[@src=\"application/view/image/icon/opencart-logo.png\"]")
	WebElement openCartLogoImg;
	
	public HomePage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}
	
	public void clickLoginLink() {
		loginLink.click();
	}

	public void clickRegisterLink() {
		registerLink.click();
	}

}
	
	

