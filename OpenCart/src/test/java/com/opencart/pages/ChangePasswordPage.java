package com.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ChangePasswordPage {
WebDriver driver;
	
	@FindBy(id = "input-current")
	WebElement currentPassword;

	@FindBy(id = "input-password")
	WebElement newPassword;

	@FindBy(id = "input-confirm")
	WebElement confirmNewPassword;
	
	@FindBy(xpath="//button[@type='submit']")
	WebElement continueButton;
	
	public ChangePasswordPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	public void enterCurrentPassword(String password) {
		currentPassword.sendKeys(password);
	}
	public void enterNewPassword(String password) {
		newPassword.sendKeys(password);
	}

	public void confirmNewPassword(String password) {
		confirmNewPassword.sendKeys(password);
	}
	
	public void clickContinueButton()
	{
		continueButton.click();
	}
	
}
