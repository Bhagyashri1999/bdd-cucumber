package com.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfirmPINPage {
	WebDriver driver;

	@FindBy(id = "input-pin")
	WebElement pinTxt;

	@FindBy(xpath = "//button[@type='submit']")
    WebElement submitButton;

	
	public ConfirmPINPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	public void setPINNumber(String pinNumber) {
		pinTxt.sendKeys(pinNumber);
	}
	public void clickSubmitButton() {
		submitButton.click();
	}
}
