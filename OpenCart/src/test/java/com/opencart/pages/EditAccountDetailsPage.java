package com.opencart.pages;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import junit.framework.Assert;

public class EditAccountDetailsPage {
WebDriver driver;
	
	@FindBy(id = "input-username")
	WebElement usernameTxt;

	@FindBy(xpath = "//select[@id='input-country']")
	WebElement countryList;
	
	@FindBy(xpath="//button[@type='submit']")
	WebElement submitButton;
	
	@FindBy(xpath="//*[@class='alert alert-success']")
	WebElement msgAlert;
	
	public EditAccountDetailsPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}
	
	public void setUsername(String username)
	{
		usernameTxt.clear();
		usernameTxt.sendKeys(username);
	}
	
	public void setCountry(String countryName)
	{
		Select selectCountry = new Select(countryList);
		selectCountry.selectByVisibleText(countryName);
	}
	
	public void clickSubmitButton() {
		submitButton.click();
	}
	
	}
