package com.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YourStorePage {
	 WebDriver driver;
		
		@FindBy(xpath= "//*[contains(text(),'Add Store')]")
		WebElement addButton;

		@FindBy(xpath = "//*[contains(text(),'Cancel')]")
		WebElement cancelButton;
		
		@FindBy(id= "input-domain")
		WebElement domainTxtBox;
		
		@FindBy(xpath = "//button[@type='submit']")
		WebElement submitButton;
		
		@FindBy(xpath = "//a[@data-original-title='Delete']")
		WebElement deleteButton;
		
		public YourStorePage(WebDriver driver) {

			this.driver=driver;
			PageFactory.initElements(driver, this);

		}

		public void clickAddButton() {
			addButton.click();
		}

		public void clickCancelButton() {
			cancelButton.click();
		}
		
		public void enterDomain(String domain)
		{
			domainTxtBox.sendKeys(domain);
		}

		public void clickSubmitButton() {
			submitButton.click();
		}
		
		public void clickDeleteButton() {
			deleteButton.click();
		}
		
		public void handlePopUpWindow() {
			
			driver.switchTo().alert().accept();
			
		}
}
