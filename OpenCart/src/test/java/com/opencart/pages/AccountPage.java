package com.opencart.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountPage {

	WebDriver driver;
	
	@FindBy(xpath = "//p[contains(text(),'Welcome to OpenCart!')]")
	WebElement validLoginMessage;

	@FindBy(xpath = "//*[contains(text(),'Edit your account details')]")
	WebElement editAccountTab;

	@FindBy(xpath = "//a[contains(text(),'Change password')]")
	WebElement changePasswordTab;
	
	@FindBy(xpath = "//*[contains(text(),'Your Stores')]")
	WebElement yourStoreTab;
	
	@FindBy(xpath = "//a[@href='https://www.opencart.com/index.php?route=account/logout' and @class='btn btn-black navbar-btn']")
	WebElement logoutButton;

	@FindBy(xpath="//*[@class='alert alert-success']")
	WebElement alertMessage;
	
	public AccountPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	public void verifyLoginMessage() {
		
		if (validLoginMessage.isDisplayed()) {
			System.out.println("User Login Successfully "+validLoginMessage.getText());
		} else {
			System.out.println("Enter valid credentials");
		}
	}

	public void clickEditAccountTab() {
		editAccountTab.click();
	}

	public void clickChangePasswordTab() {
		changePasswordTab.click();
	}

	public void clickLogoutButton() {
		logoutButton.click();
	}
	public void clickYourStoreTab()
	{
		yourStoreTab.click();
	}
	public void verifyPasswordChange() {
		if(alertMessage.isDisplayed()) {
			System.out.println(alertMessage.getText());
		}
	}
	
}
