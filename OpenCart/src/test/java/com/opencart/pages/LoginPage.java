package com.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
  WebDriver driver;
	
	@FindBy(xpath= "//*[contains(text(),' No match for E-Mail and/or Password.')]")
	WebElement invalidLoginMessage;

	@FindBy(id = "input-email")
	WebElement usernameTxt;

	@FindBy(id = "input-password")
	WebElement passwordTxt;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-lg hidden-xs']")
	WebElement loginButton;

	public LoginPage(WebDriver driver) {

		this.driver=driver;
		PageFactory.initElements(driver, this);

	}

	public void setUsernameTxt(String username) {
		usernameTxt.sendKeys(username);

	}

	public void setPasswordTxt(String password) {
		passwordTxt.sendKeys(password);

	}

	public void clickLoginButton() {
		loginButton.click();

	}
	public void verifyInvalidLoginMsg() {
		if (invalidLoginMessage.isDisplayed()) {
			System.out.println("User not able to login because "+invalidLoginMessage.getText());
		}
	}

}
