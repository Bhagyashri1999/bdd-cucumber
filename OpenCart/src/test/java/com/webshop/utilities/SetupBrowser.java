package com.webshop.utilities;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SetupBrowser {
	protected static WebDriver driver;
	
	protected static WebDriver setup()
	{
	if(driver==null)
	{
	    System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	}
	return driver;
	}

}