package com.webshop.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FileReaderUtilitiy {
	private Properties properties;
	 private final String propertyFilePath= "src\\test\\resources\\config\\application.properties";
	 
	 
	 public FileReaderUtilitiy(){
	 BufferedReader reader;
	 try {
	 reader = new BufferedReader(new FileReader(propertyFilePath));
	 properties = new Properties();
	 try {
	 properties.load(reader);
	 reader.close();
	 } catch (IOException e) {
	 e.printStackTrace();
	 }
	 } catch (FileNotFoundException e) {
	 e.printStackTrace();
	 throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
	 } 
	 }
	 
	 public String getUrl() {
		 String url=properties.getProperty("url");
		 return url;
		
	 }
	 
	 public String getDomain() {
		 String domain=properties.getProperty("domain");
				 return domain;
	 }
	 public String getDriver() {
		 String driver=properties.getProperty("driver");
		 return driver;
	 }
	 
}
