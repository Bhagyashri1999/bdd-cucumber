$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "OpenCart Login",
  "description": "",
  "id": "opencart-login",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Login with credentials",
  "description": "",
  "id": "opencart-login;login-with-credentials",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on welcome page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user navigate to login page",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "enters \u003cusername\u003e and \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "verfiy user entering \u003cPIN\u003e number",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "verify user should login successfully.",
  "keyword": "Then "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "opencart-login;login-with-credentials;",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "PIN"
      ],
      "line": 12,
      "id": "opencart-login;login-with-credentials;;1"
    },
    {
      "cells": [
        "Sai.Potdar@yahoo.com",
        "OpenCart@123",
        "2020"
      ],
      "line": 13,
      "id": "opencart-login;login-with-credentials;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 6861204300,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Login with credentials",
  "description": "",
  "id": "opencart-login;login-with-credentials;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on welcome page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user navigate to login page",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "enters Sai.Potdar@yahoo.com and OpenCart@123",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "verfiy user entering 2020 number",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "verify user should login successfully.",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_is_on_welcome_page()"
});
formatter.result({
  "duration": 5466803300,
  "status": "passed"
});
formatter.match({
  "location": "Login.user_navigate_to_login_page()"
});
formatter.result({
  "duration": 2278222200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sai.Potdar@yahoo.com",
      "offset": 7
    },
    {
      "val": "OpenCart@123",
      "offset": 32
    }
  ],
  "location": "Login.enters_username_and_password(String,String)"
});
formatter.result({
  "duration": 188388300,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_on_login_button()"
});
formatter.result({
  "duration": 4093480600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2020",
      "offset": 21
    }
  ],
  "location": "Login.verfiy_user_entering_PIN(String)"
});
formatter.result({
  "duration": 3527658100,
  "status": "passed"
});
formatter.match({
  "location": "Login.verify_user_should_login_successfully()"
});
formatter.result({
  "duration": 74539900,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 16,
  "name": "Login with invalid credentials",
  "description": "",
  "id": "opencart-login;login-with-invalid-credentials",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 17,
  "name": "user is on welcome page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user navigate to login page",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "enters \u003cusername\u003e and \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "verify user should not login successfully.",
  "keyword": "Then "
});
formatter.examples({
  "line": 23,
  "name": "",
  "description": "",
  "id": "opencart-login;login-with-invalid-credentials;",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "PIN"
      ],
      "line": 24,
      "id": "opencart-login;login-with-invalid-credentials;;1"
    },
    {
      "cells": [
        "Sai.Potdar1@yahoo.com",
        "OpenCart@123",
        "2020"
      ],
      "line": 25,
      "id": "opencart-login;login-with-invalid-credentials;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 6247675800,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Login with invalid credentials",
  "description": "",
  "id": "opencart-login;login-with-invalid-credentials;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 17,
  "name": "user is on welcome page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user navigate to login page",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "enters Sai.Potdar1@yahoo.com and OpenCart@123",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "verify user should not login successfully.",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_is_on_welcome_page()"
});
formatter.result({
  "duration": 6666165900,
  "status": "passed"
});
formatter.match({
  "location": "Login.user_navigate_to_login_page()"
});
formatter.result({
  "duration": 1839212400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sai.Potdar1@yahoo.com",
      "offset": 7
    },
    {
      "val": "OpenCart@123",
      "offset": 33
    }
  ],
  "location": "Login.enters_username_and_password(String,String)"
});
formatter.result({
  "duration": 152248700,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_on_login_button()"
});
formatter.result({
  "duration": 1342149200,
  "status": "passed"
});
formatter.match({
  "location": "Login.verify_user_should_not_login_successfully()"
});
formatter.result({
  "duration": 50324600,
  "status": "passed"
});
});